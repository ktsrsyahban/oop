<?php

include "animal.php";
include "ape.php";
include "frog.php";
$sheep = new Animal("shaun");

echo $sheep->name;
echo "<br>"; // "shaun"
echo $sheep->legs;
echo "<br>"; // 2
echo $sheep->cold_blooded;
"true";
"false";
echo "<br>"; // false

$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"
echo "<br>";
$kodok = new Frog("buduk");
$kodok->jump(); // "hop hop"